FlipMouse I2C Message Parser Library
=====================================
>**Dependencies: wire.h**


This library will serve as the api to connect your module with FlipMouse (made
for India) over I2C bus. The library will help you parse the messages from
the FlipMouse module. The library uses `wire.h` to facilitate communication over
i2c.

## How to Install the library
There are two ways in which you can install the library into your Arduino IDE.
 - #### Method 1
After cloning the repo into your local system, copy the folder and its contents
to your `Arduino/library/` and restart your Arduino Ide.

 - #### Method 2
 After the cloning the rpo into your local machine, zip the total folder and
 open your Arduino IDE. Now get into `Sketch/Include Library/Add .ZIP Library`
 and add the ziped folder.


## How to use the Library
The usage of this library is rather simple just make sure that you follow each
steps correctly.

#### Step 1
Make sure you import the module into your script

```
#include <FlipModule.h>
```
Initialise the object of the FlipModule class with the pin in which the indicator
led (compulsory) is connected.

Now initialise the wire module in the setup function. Initialise by passing the
address in which the device should be registered in the bus. And also specify the
name of the handler function which has to called upon on event of recieving data
from the i2c bus.  
```
FlipModule flip_tester(13);
...
.
void setup()
  { ..
    ..
    Wire.begin(address);
    Wire.onReceive(receiveEvent);
  }  
```  
#### Step 2
Now define the handler function which is called in event of recieving data. The
handler function will have too call the message parser function `parseMessage()`
using the initialised FlipModule object.
```
void receiveEvent(int bytes)
  {
    flip_tester.parseMessage();
  }

```
#### Setp 3
In you loop function get the x, y and SipandPuff values using the functions
`getPosX()`,`getPosY()`,`sipOrPuff` in the initialised object.
```
x = flip_tester.getPosX();
y = flip_tester.getPosY();
z = flip_tester.sipOrPuff();
```
