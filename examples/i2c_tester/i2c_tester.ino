/*  I2C message parser for FlipMouse (made ofr India)
    Copyright (C) 2018  ICFOSS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Author:
      Vaisakh Anand (gitlab: vaisakh032)
      5th Feb 2018
*/
/*******************************************************************************
 * i2c_tester.ino
 * *****************
    Example script to test the i2c message data parsing from the FlipMouse.
    Here the message from the i2c bus is parsed to using the FlipModule Library.

    Circuit:
    ********
          Leds connected to pins 5,6,3,9.
          Indicator Led connected to pin 13
          I2C Line: SDA - A2
                    SCL - A3

*/


#include <FlipModule.h>


int front = 5, right = 6, left = 3, back = 9;
FlipModule flip_tester(13);
int address = 10;
void setup()
{
 Serial.begin(115200);
 Serial.print("Initialised!");
 pinMode(13,OUTPUT);
 pinMode(front,OUTPUT);
 pinMode(back,OUTPUT);
 pinMode(right,OUTPUT);
 pinMode(left,OUTPUT);
 Wire.begin(address);
 Wire.onReceive(receiveEvent);

 digitalWrite(back,HIGH);digitalWrite(front,HIGH);
 digitalWrite(right,HIGH);digitalWrite(left,HIGH);
 delay(1000);
 digitalWrite(back,LOW);digitalWrite(front,LOW);
 digitalWrite(right,LOW);digitalWrite(left,LOW);

}

void receiveEvent(int bytes)
{
  flip_tester.parseMessage();
}

int x,y,z;
void loop()
{
  x = flip_tester.getPosX();
  y = flip_tester.getPosY();
  z = flip_tester.sipOrPuff();

  if((y < 5)&&(y < 0))
    {Serial.println("Front");digitalWrite(front,HIGH);digitalWrite(back,LOW);}
  else if(y > 5)
    {Serial.println("Back");digitalWrite(back,HIGH);digitalWrite(front,LOW);}
  else
    {digitalWrite(back,LOW);digitalWrite(front,LOW); }


  if((x < 5)&&(x < 0))
    {Serial.println("Left");digitalWrite(left,HIGH);digitalWrite(right,LOW);}
  else if(x > 5)
    {Serial.println("Right");digitalWrite(right,HIGH);digitalWrite(left,LOW);}
  else
  {digitalWrite(left,LOW);digitalWrite(right,LOW);}

  if(z==1)
  Serial.println("Sip");
  else if(z==2)
  Serial.println("Puff");

  x=0;
  y=0;
  z=0;
}
