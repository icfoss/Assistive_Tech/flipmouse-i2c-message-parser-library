/*  I2C message parser for FlipMouse (made ofr India).
    Copyright (C) 2018  ICFOSS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Author:
        Vaisakh Anand (gitlab: vaisakh032)
        5th Feb 2018
*/

#ifndef FlipModule_h
#define FlipModule_h

#include "Arduino.h"
#include "Wire.h"

/*
  Class that contains the variable required to parse the message from the object.
*/
class FlipModule
{
  private:
    int _indicatorLed,_x,_y,_sipPuff;
    int _xVal, _yVal,_pVal;
    char _input;

  public:
    FlipModule(int ledPin);
    int parseMessage();
    int getPosX();
    int getPosY();
    int sipOrPuff();
};

#endif
