/*  I2C message parser for FlipMouse (made ofr India).
    Copyright (C) 2018  ICFOSS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Author:
        Vaisakh Anand (gitlab: vaisakh032)
        5th Feb 2018
*/

#include "Arduino.h"
#include "FlipModule.h"


/*
  Constructor to initialise the FlipModule object.
*/
FlipModule::FlipModule(int ledPin)
{
  _xVal = 0; _yVal = 0;
  _x = _y =0;
  _indicatorLed = ledPin;
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin,LOW);
}

/*
  Function to parse message recieved from FlipMouse.
*/
int FlipModule::parseMessage()
  {
    while (Wire.available())
      {
        if (_xVal)
          {_x = Wire.read();_x-=100;_xVal = false;}
        else if (_yVal)
          {_y = Wire.read();_y-=100;_yVal = false;}
        else if (_pVal)
          {_sipPuff = Wire.read();_pVal = false;}
        else
          {
            _input = Wire.read();
            if(_input=='#')
              _xVal = true;
            if(_input=='@')
              _yVal = true;
            if(_input=='*')
              _pVal = true;
          }

        if(_input=='s')
          digitalWrite(_indicatorLed,LOW);
        else if (_input=='p')
          digitalWrite(_indicatorLed,HIGH);
      }
     _input = '/0';
  }

  /*
    Function to return the x value from the parsed message.
    values: Front =  less than 0
            Back = greater than 0
  */
int FlipModule::getPosX()
{
  int temp = _x;
  _x = 0;
  return temp;
}

/*
  Function to return the y value from the parsed message.
  values: Left =  less than 0
          Right = greater than 0
*/
int FlipModule::getPosY()
{
  int temp = _y;
  _y = 0;
  return temp;
}
/*
  Function to return the sip and puff value from the parsed message.
  values: Sip  = 1
          Puff = 2
          else 0
*/
int FlipModule::sipOrPuff()
{
  int temp = _sipPuff;
  _sipPuff = 0;
  return temp;
}
